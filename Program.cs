﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr_stringcalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Friendlyrentals string calculator kata";
            Console.WriteLine("Follow instructions on 'readme.md'");
            Console.ReadLine();

            Console.WriteLine(Add(""));
            Console.WriteLine(Add("1"));
            Console.WriteLine(Add("1,2"));
            Console.WriteLine(Add("1,2,3,-1"));
            Console.WriteLine(Add("B"));
            Console.WriteLine(Add("1,B"));
            Console.WriteLine(Add("B,1"));
            Console.WriteLine(Add("A,B"));
            Console.WriteLine(Add("A,B,1,2"));
            Console.WriteLine(Add("1,2\n3"));
            Console.WriteLine(Add("1,2\n3,4"));
            Console.WriteLine(Add("1,2\n3,4\n,B"));
            Console.WriteLine(Add("//;\n1;2"));
        }

        static int Add(string numbers)
        {
            int result = 0;
            int numberparse = 0;
            string[] arrayNumbers;

            if (String.IsNullOrEmpty(numbers))
            {
                result = 0;
            }
            else
            {
                string limitador = "";

                arrayNumbers = numbers.Split('\n');

                if (arrayNumbers[0].Contains("//"))
                {
                    limitador = arrayNumbers[0].Trim('/');
                    arrayNumbers = arrayNumbers[1].Split(limitador.ToCharArray());
                }
                else
                {
                    arrayNumbers = numbers.Split(new char[] { ',', '\n' });
                }


                for (int i = 0; i <= arrayNumbers.Length - 1; i++)
                {
                    if (int.TryParse(arrayNumbers[i], out numberparse))
                    {
                        try
                        { 
                            if (numberparse < 0)
                            {
                                throw new Exception("No se pueden utilizar números negativos.");
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);

                            return result;
                        }
                    }

                }
            }
            return result;
        }
    }
}